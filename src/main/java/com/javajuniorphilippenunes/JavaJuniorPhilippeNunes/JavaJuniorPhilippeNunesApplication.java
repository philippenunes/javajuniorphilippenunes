package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaJuniorPhilippeNunesApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaJuniorPhilippeNunesApplication.class, args);
	}
}
