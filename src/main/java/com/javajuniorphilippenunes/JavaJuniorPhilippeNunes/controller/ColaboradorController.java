package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.controller;

import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception.ColaboradorExistenteException;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception.ColaboradorNotFoundException;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception.LimiteMaiorSessentaECincoException;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception.LimiteMenorDezoitoException;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.model.Colaborador;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.model.Setor;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.service.ColaboradorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

/**
 * Created by Nunes on 25/05/2019.
 */

@RestController
public class ColaboradorController {

    @Autowired
    private ColaboradorService colaboradorService;

    @ResponseBody
    @PostMapping(value = "/colaboradores/{idSetor}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity inserirColaborador(
            @RequestBody Colaborador colaborador, @PathVariable int idSetor) throws Exception {

        try {
            return new ResponseEntity<>(colaboradorService.inserirNovoColaborador(colaborador, idSetor), HttpStatus.CREATED);
        } catch (LimiteMenorDezoitoException e) {
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        } catch (LimiteMaiorSessentaECincoException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        } catch (ColaboradorExistenteException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/colaboradores/{cpf}")
    public Colaborador obterColaborador(@PathVariable String cpf) {

        try {
            return colaboradorService.obterColaboradorPorCpf(cpf, false);
        } catch (ColaboradorNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/colaboradores")
    public ResponseEntity<Collection<Setor>> listarColaboradores() {
        Collection<Setor> listaColaboradores = colaboradorService.obterColaboradoresAgrupadosPorSetor();
        return new ResponseEntity<>(listaColaboradores, HttpStatus.OK);
    }

    @DeleteMapping("/colaboradores/{id}")
    public ResponseEntity removerColaborador(@PathVariable int id) {
       colaboradorService.removerColaborador(id);
       return new ResponseEntity(HttpStatus.OK);
    }
}
