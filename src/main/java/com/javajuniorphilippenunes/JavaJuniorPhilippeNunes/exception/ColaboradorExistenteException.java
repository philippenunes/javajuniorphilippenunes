package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception;

/**
 * Created by Nunes on 26/05/2019.
 */
public class ColaboradorExistenteException extends Exception {

    private String message;

    public ColaboradorExistenteException(String message) {
        super(message);
        this.message = message;
    }
}
