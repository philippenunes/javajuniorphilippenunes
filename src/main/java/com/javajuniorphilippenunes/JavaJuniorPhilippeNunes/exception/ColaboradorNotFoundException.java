package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception;

/**
 * Created by Nunes on 26/05/2019.
 */
public class ColaboradorNotFoundException extends Exception {

    private String message;

    public ColaboradorNotFoundException(String message) {
        super(message);
        this.message = message;
    }
}
