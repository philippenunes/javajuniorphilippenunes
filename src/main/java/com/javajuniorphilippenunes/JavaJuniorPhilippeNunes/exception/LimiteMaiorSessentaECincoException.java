package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception;

/**
 * Created by Nunes on 26/05/2019.
 */
public class LimiteMaiorSessentaECincoException extends Exception {

    private String message;

    public LimiteMaiorSessentaECincoException(String message) {
        super(message);
        this.message = message;
    }
}
