package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception;

/**
 * Created by Nunes on 26/05/2019.
 */
public class LimiteMenorDezoitoException extends Exception{

    private String message;

    public LimiteMenorDezoitoException(String message) {
        super(message);
        this.message = message;
    }
}
