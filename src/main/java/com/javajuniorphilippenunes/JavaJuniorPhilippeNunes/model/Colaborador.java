package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.lang.NonNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by Nunes on 25/05/2019.
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document(collection = "colaborador")
public class Colaborador {

    @Transient
    public static final String SEQUENCE_NAME = "colaborador_sequence";

    @Id
    @NonNull
    private long id;

    @NonNull
    private long idSetor;

    @NonNull
    private String name;

    @NonNull
    private String cpf;

    @NonNull
    private String email;

    @NonNull
    private String telefone;

    @NonNull
    private String dataNascimento;

    @NonNull
    private int idade;
}
