package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.model;

import lombok.*;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

/**
 * Created by Nunes on 25/05/2019.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "colaborador_sequences")
public class ColaboradorSequences {

    @Transient
    public static final String SEQUENCE_NAME = "users_sequence";

    @Id
    private String id;

    private long seq;
}
