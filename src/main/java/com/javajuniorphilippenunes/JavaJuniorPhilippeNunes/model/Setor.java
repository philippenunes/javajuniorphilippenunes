package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.model;

import com.mongodb.lang.NonNull;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

import java.util.Collection;
import java.util.List;

/**
 * Created by Nunes on 26/05/2019.
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "setores")
public class Setor {

    @Id
    private long id;

    @NonNull
    private String descricao;

    private Collection<Colaborador> colaboradores;
}
