package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.repository;

import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.model.Colaborador;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Collection;

/**
 * Created by Nunes on 25/05/2019.
 */

public interface ColaboradorRepository extends MongoRepository<Colaborador, Integer> {

    Colaborador findByCpf(String cpf);

    Collection<Colaborador> findAllByIdSetor(long idSetor);

    Collection<Colaborador> findByIdSetorAndIdadeLessThan(int idSetor, int idade);

    Collection<Colaborador> findByIdSetorAndIdadeGreaterThan(int idSetor, int idade);

}
