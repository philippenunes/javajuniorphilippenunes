package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.repository;

import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.model.Setor;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Nunes on 26/05/2019.
 */

public interface SetorRepository extends MongoRepository<Setor, Integer> {
}
