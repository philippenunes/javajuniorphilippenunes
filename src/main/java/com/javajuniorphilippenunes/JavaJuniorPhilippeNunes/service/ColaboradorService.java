package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.service;

import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception.ColaboradorExistenteException;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception.ColaboradorNotFoundException;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception.LimiteMaiorSessentaECincoException;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.exception.LimiteMenorDezoitoException;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.model.Colaborador;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.model.ColaboradorSequences;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.model.Setor;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.repository.ColaboradorRepository;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.repository.SetorRepository;
import com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Iterator;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * Created by Nunes on 25/05/2019.
 */

@Service
public class ColaboradorService {

    @Autowired
    ColaboradorRepository colaboradorRepository;

    @Autowired
    SetorRepository setorRepository;

    @Autowired
    MongoOperations mongoOperations;

    public Colaborador inserirNovoColaborador(Colaborador colaborador, int idSetor) throws Exception {
        Colaborador colaboradorExistente = obterColaboradorPorCpf(colaborador.getCpf(), true);

        if(colaboradorExistente != null) {
            throw new ColaboradorExistenteException(Util.COLABORADOR_JA_CADASTRADO + colaboradorExistente);
        } else {
            int idade = calculaIdade(colaborador.getDataNascimento());

            if(limiteMenoresDezoito(idSetor) && idade <= Util.DEZOITO_ANOS) {
                throw new LimiteMenorDezoitoException(Util.LIMITE_MENORES_DEZOITO);
            }

            if(limiteMaioresSessentaECinco(idSetor) && idade >= Util.SESSENTA_E_CINCO_ANOS) {
                throw new LimiteMaiorSessentaECincoException(Util.LIMITE_MAIORES_SESSENTA_E_CINCO);
            }

            Colaborador newColaborador = Colaborador.builder()
                    .id(generateSequence(Colaborador.SEQUENCE_NAME))
                    .idSetor(idSetor)
                    .name(colaborador.getName())
                    .cpf(colaborador.getCpf())
                    .email(colaborador.getEmail())
                    .telefone(colaborador.getTelefone())
                    .dataNascimento(colaborador.getDataNascimento())
                    .idade(idade)
                    .build();
            colaboradorRepository.save(newColaborador);
            return newColaborador;
        }
    }

    public Colaborador obterColaboradorPorCpf(String cpf, boolean newColaborador) throws ColaboradorNotFoundException {
        Colaborador colaborador = colaboradorRepository.findByCpf(cpf);
        if(colaborador == null && !newColaborador) {
            throw new ColaboradorNotFoundException(Util.RECURSO_NAO_ENCONTRADO);
        }
        return colaborador;
    }

    public Collection<Colaborador> obterListaDeColaboradores() {
        return colaboradorRepository.findAll();
    }

    public Collection<Setor> obterColaboradoresAgrupadosPorSetor() {
        Collection<Setor> setores = setorRepository.findAll();
        for (Setor setor : setores) {
            Collection<Colaborador> colaboradores = obterListaDeColaboradoresPorSetor(setor.getId());
            if (colaboradores != null) {
                setor.setColaboradores(colaboradores);
            }
        }
        return setores;
    }

    public void removerColaborador(int id) {
        colaboradorRepository.deleteById(id);
    }

    public Collection<Colaborador> obterListaDeColaboradoresPorSetor(long idSetor) {
       return colaboradorRepository.findAllByIdSetor(idSetor);
    }

    private int calculaIdade(String dataNascimento) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate date = LocalDate.parse(dataNascimento, formatter);
        int anoAtual = LocalDate.now().getYear();
        return (anoAtual - date.getYear());
    }

    private boolean limiteMenoresDezoito(int idSetor) {
        Collection<Colaborador> totalColaboradores = obterListaDeColaboradoresPorSetor(idSetor);
        Collection<Colaborador> totalMenoresDezoitoAnos = colaboradorRepository.findByIdSetorAndIdadeLessThan(
                idSetor, Util.DEZOITO_ANOS+1);
        int percentualDoTotal = (20*totalColaboradores.size())/100;
        return percentualDoTotal == totalMenoresDezoitoAnos.size();
    }

    private boolean limiteMaioresSessentaECinco(int idSetor) {
        Collection<Colaborador> totalColaboradores = obterListaDeColaboradoresPorSetor(idSetor);
        Collection<Colaborador> totalMaiorSessentaECinco = colaboradorRepository.findByIdSetorAndIdadeGreaterThan(
                idSetor, Util.SESSENTA_E_CINCO_ANOS-1);
        int percentualDoTotal = (20*totalColaboradores.size())/100;
        return percentualDoTotal == totalMaiorSessentaECinco.size();
    }

    /**
     * Gera uma nova sequence para um novo colaborador
     * @param seqName
     * @return sequencie
     */
    public long generateSequence(String seqName) {
        ColaboradorSequences counter = mongoOperations.findAndModify(query(where("_id").is(seqName)),
                new Update().inc("seq",1), options().returnNew(true).upsert(true),
                ColaboradorSequences.class);
        return counter.getSeq();
    }
}
