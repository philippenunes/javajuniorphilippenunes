package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes.util;

/**
 * Created by Nunes on 26/05/2019.
 */
public class Util {

    public static final int DEZOITO_ANOS = 18;
    public static final int SESSENTA_E_CINCO_ANOS = 65;
    public static final String LIMITE_MENORES_DEZOITO = "Limite de colaboradores menores de 18 anos atingida neste setor! ";
    public static final String LIMITE_MAIORES_SESSENTA_E_CINCO = "Limite de colaboradores maiores de 65 anos atingida neste setor! ";
    public static final String COLABORADOR_JA_CADASTRADO = "Colaborador já cadastrado no sistema! ";
    public static final String RECURSO_NAO_ENCONTRADO = "Recurso não encontrado. ";
}
