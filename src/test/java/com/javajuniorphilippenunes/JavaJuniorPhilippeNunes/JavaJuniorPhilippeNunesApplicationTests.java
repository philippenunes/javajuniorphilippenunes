package com.javajuniorphilippenunes.JavaJuniorPhilippeNunes;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JavaJuniorPhilippeNunesApplicationTests {

	@Test
	public void contextLoads() {
	}

}
